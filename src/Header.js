import React from "react";
import SearchIcon from "@mui/icons-material/Search";
import ShoppingBasketIcon from "@mui/icons-material/ShoppingBasket";
import "./Header.css";
import { Link } from "react-router-dom";


function Header() {
  return (
    <div className="header">
      <Link to="/">
      <img
        className="header_logo"
      
        src="http://pngimg.com/uploads/amazon/amazon_PNG11.png"

        alt=""/>

      </Link>
      
      <div className="header_search">
        <input className="header_searchInput" type="text"></input>
        <Link to="https://www.amazon.in/">
        <SearchIcon className="header_searchicon" />
        </Link>
      </div>

      <div className="header_nav">
        <div className="header_option">
          <span className="header_optionlineone">Hello Guest</span>

          <span className="header_optionlineTwo"><Link className="Signinlink" to="/signin">Signin</Link></span>
        </div>

        <div className="header_option">
          <span className="header_optionlineone">Returns</span>

          <span className="header_optionlineTwo">Order</span>
        </div>

        <div className="header_option">
          <span className="header_optionlineone">Your</span>

          <span className="header_optionlineTwo"><Link className="primelink" to="/prime">Prime</Link></span>
        </div>

        <div className="header_optionBasket">
          <ShoppingBasketIcon />
          <span className="header_optionLineTwo header_basketCount">0</span>
        </div>
      </div>

      
    </div>
  );
}

export default Header;
