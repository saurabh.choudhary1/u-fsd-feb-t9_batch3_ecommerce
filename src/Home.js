import React from "react";
import "bootstrap/dist/css/bootstrap.css";
import Carousel from "react-bootstrap/Carousel";
import "./Home.css";
import Product from "./Product";
import Header from "./Header";
import Tvmini from "./Tvmini";

function Home() {
  return (
    <>
      <Header/>
      
      <div className="carousel">
        <Carousel>
          <Carousel.Item interval={1500}>
            <img
              className="d-block w-100"
              src="https://pbs.twimg.com/media/FTLe4bhaQAAuVhX.jpg"
              alt=""
            />
          </Carousel.Item>
          <Carousel.Item interval={800}>
            <img
              className="d-block w-100"
              src="https://techcrunch.com/wp-content/uploads/2022/07/Top-10.png?w=730&crop=1"
              alt=""
            />
          </Carousel.Item>

          <Carousel.Item interval={800}>
            <img
              className="d-block w-100"
              src="https://www.91-cdn.com/hub/wp-content/uploads/2023/06/Amazon-Prime-Day-sale.jpg"
              alt=""
            />
          </Carousel.Item>
        </Carousel>
      </div>

      <div className="home">
        <div className="home_row">
          <Product
            title="Back to college"
            price={570}
            image="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBYVFRgVFRYYGRgaGRgaHBoaGBocGBwcGBgaGRgYGBgcIS4lHB4rIRoaJjgnKy8xNTU1GiQ7QDszPy40NTEBDAwMEA8QHxISGjQrISs0NDQ0NDQ0NDQ0MTE0NDYxNjQ0NDQ0NDQ0NDQ2NDQ0NzQ0NDQ0NDQxNDE0NDQ9NTU0NP/AABEIATIApQMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAAAAQIDBAUGBwj/xABHEAACAQIDAwcIBwYEBgMAAAABAgADEQQSIQYxQQUTIlFhcYEHFDKRobHB0SNScoKSwvAzQmKisuEkQ3PxFRYXY4PDU7Pi/8QAGgEAAgMBAQAAAAAAAAAAAAAAAAECBAUDBv/EADARAAIBAgQCCAYDAQAAAAAAAAABAgMRBBIhMVFhExQiMkFxgaEFIzM0UpFDRLEV/9oADAMBAAIRAxEAPwD2aEIQAIQhAAhCEAEiQmbjeW6FK4eqoI/dBu34VufZE2luNRk3ZK5picZtryxX5yjyfg3CYmuHYuRfm6aoxzfwlmUgNY2ynjaLi9u6SG6oxQG7MxAso3lVFyxtfQ2mD5PcS2O5RxeOYEBUWnTHBUdjlXvC07ntduuJSjLZk50pwtmVrml5KuWqlWg+FxGbzjCvkYN6WQ3yX6yCGXuVTxnfTi9qsH5rXXlWkNUATFKP38OSAXtxdLK3aFtfQSztNtX5tzXNotQVFLBi9ly6WtYG973v745SUVdkYQc5ZYrU6q8WcHh/KOh/aUHX7Lq/vCzXwu22Dfe7Iep0YfzAFfbIqpF+J0lhqsd4s6WEp4PlKjV1p1Uf7Lq3uMuSdzk01uLCEICCEIQAIQhAAhCEACEIQAbecby/tTVpVXo00UZbAs12Juoa4UWA0I3k907K08924w4XEZgLZ0BPaykqT32yjwE415SjG8S3gYQnVyzVzHxnK1er6dViOoHKviq2B8ZRNgNBEjKrBQWY2ABJPAAC5Pqme25PU9AoQprRWRhbT42wWkOPSb7IPRHiQT90dc9O8kXJ/N4DnDvrVHfX6q2pqO7oE/eniGKxJqOznexvbqG4DwAA8J7l5JcfzmAFPjRd016jaoPDp2+7NGlHLFI87iajqycvA7DHYVatN6Ti6urI32WBU+wz52wlZ87UKzFqmHvRF/qUmZQo7Fa/gRPo93ABJ3AEnuG+fLXKmKZcU9caM7mrY8TUuzqewksI6izKwsNJwlntotH6m3zxBtLlOp1zPaqpKuvosLjrB4g9oOh7peK6SlJG/DXVE4YHUgT1fYLN5hQZmZywZgWvoruzKovwVSFHYNNLCeN1WbK2UXaxAHEsdFHiZ7/ydhRSpU6S7qaIg7kUKPdO+HW7M34m+6i3CEJaMoIQhAAhCEACEIQAIQhABpnJbf0bpTbqZl/Eub8k66YG2YHmrMf3WQ+tgp9jGc6yvBljCyy1ovmeZVqmXQb5h7R40rSybi5t91bFvyjxmth6pOYim7E7rqVH4msBM7lXkCrXIZ6oWwsFVSyqDvsSRc6C+koU8qknI38SpypuMFds40G89O8jHKBXEVqDH9pTDj7VNrG3aVcn7k5J9kiP80Hr6FvzGbOzeHOEr0qxsTTYmwvmIIKMLnQdFiJadaF1ZmXHBVMjUlZnsu09XLhK5vYmmyg9rDKPaZ83bQD6RWtvBHqN/jPXtptqVxFAJTVku12D5RcKDYdEt+9Y8NwnnPKfJbVgBmVSCTc57WsRb0d97eqKVWOdO+hKnhpLDu61b28jBwGKy9HgTmXsbj6wPZ2zsqaZ0DDfacuNna4OmXTcQx3jcRcTpBiObsWGUEKWAF1DEDMA2617znWytpxZZwWeMXGaattcsbN4c1cXh6Z41lY91I88QewhCPGe7ieReTxVqcoFgOitKo477009ztPXJYoLsmf8Qletbgh0IkWdiiEIQgAQhCABCEIAIZFVqqoJYgAakk2A7zIsbi1pKXY2A9ZPAAcSZwHLHKr1muxsAeio9Edp627fVacatVQXMtYbCyrvTRcTW5V2sa5WgAB9dhcntVTu8b9wnM4rHVKhu7M32mJA7huHgBIit/nEyDj+v14TPnWlLdm7Sw1KktFrx8RCYc2eo+4e2Lzqr/aw9u/2yJ8ZxAHfvPrnLU73ZIaN+I9/ukGLTIjMDdgpIGQm5A04i/dcd8Z525Ppe6QYh7ggsdeonSSinfUUk2nqc7Q5WxJdcypqSCrAKlrCzZwSTe99NO++iDEVRiFaqLalAF9Eq17FQpPWL3B9FerSHCVVNRVuhsugGU9EdJTooA0OgvcWN7G81+X8OOazqbEP/EAMrFb9HiOGhl1tKWWy1MqEZSpuWZtp330LQqdh9lvbY+yLe/H2GVMLVL21IuJdZLbz8ZWasacXdE3I+ObCVDUoBAxGUgoLEEgkECx3gbiN09D5B20pVyKdUc1UOgubox4ZW0sT1G2+wJnmLkjh+u4yB9bj2bj+EzrCrKPkVa+EhV1tZ8T6BizybZTbF8Oy0q7F6O4MblqfAEcWXs4cN1j6pSqKyhlIIIBBBuCDqCCN4l6E1NXRiVqEqUrSJoQhJHEIQhABI1jbUxwmBtXjslLIN73H3R6XvA+9ITkoxbZOlB1JqK8TnOXuVTWfQ2QXyj8x7T7B4zMKW1O/q+fyirvvxO7s/iPwkDvmv9X3zJnNzldnpadNQioR2RHUrdW7r+A7JGQWU2NjbfvseF+uOdcxy6i/EA+32yVhYWG7j1mJI66JaFFcqnpZtBqL2v4jW+nX4SjUJJJtbXQAaS/VXWQsk6ICJVAUXF9NdTvkOIUFGHokgi4uSAQRoCbcZaA/WsY1K++CdmJq6szluTeSWNZmG7XU6E332tYDwnS8r4G2HIS97Am+u83a4trx9csUKIXWPxFS4kpVJSknwOcaEIxcUtHc53knD9HMzNqdBcaLwBuu/tmyaot/cfKViotGxyeZ3Y4xUI5VsNxLMTdbC3A7z97X1SCpV4vmX7W7ss3o37jLOe26RPXJ04dUENrmMFYE2AY7zwOgtfX53751+xu1Rw5FKoS1AnQ7zTJOpHWt944XuOo8S7ZA2QWuLEWHo3uch3ju3G0fhqmlwf8AYycW4u8ThVpxqxyyR9EU3DAMCCCAQQbgg6gg8RJJ5z5PuXrHzVzodad+B3sncd48RxE9Gl6E1JXRg1qTpTcWLCEJI5CGefbR4rncQwv0U6P4fS8SxI+7O6xlcIjudyqzHuUE/CeY5tCzan0m7WOvx9ZlPFztFRXiaXw6F5uXDT9jcQ53cTv7uAkDvlihr6neZEdTbr/RlFKxuJWRPSWxJPcPjHut4oWLaMg2UWHTI6pWc62keJxQSqFNgHdVuTZVVmAd2PUoIMoYLlhXbeEbonIXBJDozAqdbgZdeq6njp1jSk45ktDi8TTjPI3qa6U4xyL6R1apY2mdjcUqDMxsOJkVFs7OSSuy3iK4VLkgX67aDrMxqPLVKo2RSb8CRZW+yfnaZxrPiGDMMtIdJQ4JVyrDosq6ldDoLbt943E4amVIUU1cWIZOcUiwXcjHUkL62J4y3DDdnXcy6vxK00oq68eZvMZGxlDknlHnFyPo66EHebcbS8ROMouLszRhUjUipR2I6jyG0mIkZMSGyPLGKtiBwY27jqfmf95IzSKr6LHqF/w9L4SSIM1abkEOhIKkajeLHQjqI3z2bZjlYYmgtTTMOi4HBhv04XBB8Z4lgHuovvtrO48nnKGSu1Eno1Bp9pLkW71zepZOjLLK3Eq42kp08y3WvoenQhCXTDMLa6tlwzjixVe8E3YfhDTz7FPuXq1Pef17Z2m279GkvW5b8IA9zGcCz5iT1m8zsS7z8kb3w2NqV+LEdyN0kwzhje/+/VIjFpDW9tb6+E4GjLY0FMc5kdOLUMRxRym0j5XQ3tYVSdL6WQk2Oh0BnO4F1GJQh84Jaz5cpKqrJT6OuU2FyLm1wOE63lKlmxFBcoe5a6N6LA+kpIvoQDfs9c4zEhRXAQWVWJUZWVkS5OR1YmxW997ekbs3DRpfS/Zi1/uvVHd5L6k75m8rcnLWsCzDKSeiRY3FtQRr/cy6lbMARu4RJTTcXdG1KMZxyyWhjLyVWQWSuwAGUZkRjl16Nzrl1Om7WJzGKAI59TcZdaaiwuScoGgNyTNljGFhOyrz4lV4Gi/D3OcqcnVzU5wsgbTdmGgAFrWPV7ZrozEDNv42lljI7SMpuW51pUY0k1HYiYSMiTMI0iRR1ISsRV1947I8iIIxC4AZVtxBK+rQzXweINN1qLvVlYduUg27tJl4Zek/2veA3xlxDDxFa8bM94oVAyhhuIBHcRpFmPsjX5zCUT1Ll/ASn5YTQTujzU1lk0YW3mI6YX6qX/G1vcpnGzc2xrXxLjqI/lRfiWmKJmVXebfM9Jgo5aMVy/3UrNWymzadstYc6cDfW44xlRQ2n68JPSQ2HhOZZlsT05IwjEWDmI5Iw8awbG0FLugCls9P01KioVKDicyAW43t3cnXrg1y5YtnDEuwsXLdLnGXMwDG2tmOonTVaypj6DuwVETMzF2QBVFZm6aAsDYWAAuxIXTNcYOKpl3epc5KorVaYLXYA1yrEjh0g47ct5pQ+kvIxKn3T8zY5Ia9MS4xmZyCTkA7JpOZTktTbi7xRGzSNjJCIwiAxhJjGMlKxCsYiGF5IRGOQIAMaNGkTNH3jIsWm2/t+QHwlunKSEA+A95lqlExo9W8n9S+EA+q7j1nN+aEr+Tc/wCHqf6x/wDrpxJdh3UeexCtVl5nK8t1M2JrHqdh6mb4Slmlvl1MuJr/AOox9Zv8ZQvKE1qz0dD6cbcESEBtBo364cRLSvKYJ0uL66EHd4S0g7ZzJTJlaNbWKEiNpEQRyW0bMK4ZQbrSVrjeoV6l2DEHIRvDW6Jsd8wOhpzSkBKbBmzX5xucuXsFGUEFQF1sFFyZ0nKdVkxOZUL2o9IA1FIXO+Zs1JlZbDje2utwdadHEriK5bKVvSIYNkJzCoupdETNoR0mBbgSbCaUdKS8jDkr4trmTchMQg7ppkSPC4XILCTNKcndm5GNlYiMQxXfSQmCGOZ5GzxSsaxjEJeRtaKTGEwsIAvVDUQBktri0YESWzX7Bp4tLdKUqaWfwHvaXFOsTEj0rya1Po6y9Tq34lt+WEoeTp7Gv3Uv/ZCW6b7KMXE030sjJ2gYHEVv9Rh6jaZgAO+W+WDevW7atT+tpS3ayjPvM3qCtTiuSHJod4PZx9ctI8rpYkEWPqv7ZOtpElIto4I0jGaNU6aRQJEgZuDoK/KC5hmUYcsVyB8wDvdchpve+Y7lB32KylURPPHyBFXm1GVKYpopOUlcmVTfTUkXPG8spXopji1d0RBQAVnyGziqrdHOjAMUFRb2uAzWN98Qsca4Du4FKiQ7sWLXo02zgk6K2cEAaAEW0mj/AA+hiR+79S5zdpXcyziW0tcym0pI3BjGREyRjIiZJCGkxjGPtEKxiZCxjSJMacaUhcREJLSOsYVgojAlc9IddiPUR85Imkizajr1Hrt8pKqxMEddsXf6W3VTv/PCS7Cf53dS/PCd4PsozMQ/mMx+VP21X/Uqf1NKyvY2k/KJ+mqH+N/6zK7JfjaVpbmvS7i8h6DpbiOsX6JlhUEr4dGB1ser+8uoJBjkPVhbWOEFpxxkSBV5IX/HVOll/wAMNefaiTaqjZc6Akg2IK23X3ETLqi3KFcMzMQiDM1TO7ZaeHUM7kDM53nQWJIGglyhRpNia5rUlqImHViGDkrdlW6IiOxJuFzADKCTmGl8XC4IYfG1KIzWRSBc3ax5pwSQq7wwPogi9iAbzR/h9DDX3fqa+I1lRpcxAvKbiUkbo2MIj40iSEMKxpkl40mMTGFpGzR7GNtABl4ayVUjmAhcCK+7v+B+UnEidRoRvv8AA8I9ReDEjrdjT+1+5+eEi2UYDnPufmhO0XoUKy7bM/Hn6V/tv/UZBcyxyiLVXH8b/wBRkBeVpbmpT7qJKA1l6k0pUCb6i3ql2lIMckWIzNB2AEjzREEjOp4xUxFfNUpUy1FcrVVdhZXVnyZWVi+XNlUEZrWmLQK+euUZGUjMGQEIc4pubAu5zXJDdNukG1tNQ5mr4hESlUepQRVSrkyuQ6OQoZ0u1luLXOl+2ZOEpsmMZKlNabjMGppYopyjRSGYdp6R1J3bhor6PoYP9v1N17WlR1l11tvlOpv3ykjfIrRCI+0MokhEVopkmWKUgIhyRebilO2Je0AEK2kbDWK7xgMYmK66DvHtjwZE7aeI9hF5MICN/Z0/tPu/mixNnx6f3fzQneOxRq99kXKq2r1R/wBx/wCoyIOOqXdpUy4qsP4yfxHN8ZmAytNas0aLvTi+SLNLfrLdNJUw/wAZaRSOM5snIe0iq3ksZiDZCeyIiVuRnfzuuKYu7YVQA2bmmBZM4qsiMwBF7aWLaE664QC/8QqhL5VZ7AoEsQqqy5AiZbNcegpNrkXJmwKYsSMvTUIwdEqIwRsyBkdSNDcgixkKUTz71mYO9S2Y5AgFhlACgm2iroDwl1Vo9Hl8bWMnqlTrGe2l7j6zXldgJZsALb5Ew7JVRrkVhACO8I1jJCC0Y1SI4kDExiZKzyJ6sYTEtGRY0mLfSSrSvxjiqr3wuFiuUJ7P7yxRa4vGhrww263USPUSPhGLxOu2QS/O/c/PCW9g0/bacKX54TtBdlGbXfzGQbb0suLc/WCt/KB7wZghL8dJ2PlCw/0qP1pb8LE/mnFOxFuqcaqtJmhgpZqMfL/C5h1lsCVMMbt90e0sZav1zgywxWkGJNwRJmlesbRILEaiy2kCyxvBEq6lpNBYsWkL75NTN7jqlSq5zGCEPkLtHl5Cz8YwY1zeRMsmI0vEyyRErlYhWWSsbkhcLEKqZIlLrj1SPFhxtC4rCEgSKl6Td/vAPxg5BNhFo72+7/SsaEz0Dycr+3/8X/shJvJtT6NZusoPUGP5oS1TXZRh4qXzWXtvafQpv1MV/EAfymed1nCAuxARb3v6h362nqe2lMHCux/cKt3C+Un1EzwXlfHmqxy3yA9EcCdRnI6zc26ge0yFSk5VORYw2LVLD87tJGvR2gpZmvmW2XKcpJOh16N7a8DLh2kocS1/sN8u2cXQ1Z+zLp4SRpJ4aD4nP/o1uX6OsqbTUNLZzp9X2akSniNpUO5H8co495nO2gyxrDQRF/EKz8V+jZfaRgulPXQ6vr7B3cZE20D5swprrwznf35e+ZjLeNaSVGHA5vHV/wAvY122mdWJFNLt/E2ns10lartA43on4m4zJqNIajXj6CHAXXa/5exsrtI9x9Gmn8TRo2ia1ubXff0j6t0xIWj6KHAOu1/y9jdO0TWtzY/EfH92ObaVj/lD8R9+WYMIuhhwH12v+Xsbx2jvoaQ8H/8AzHjaQcaRHc4+QnPQvF0MOALHV/y9kdCdol/+N/Wsa3Lyn9x/5fnMCOEOhhwJdercfY3f+MpY2V+HVft4zUwNcOMwBAJ3Hfppr6pyayzhMY9NiVPR0JU7jpbTqMjOirdnc6UsdLN29j3jybD6Cof+7b1Ih+MJJ5NQDgw41Du7A91k96keEJOEeyipiJJ1ZPmbvL3J3nGGrUL25ymyg9RI08L2nzQ91NiCDuIO8HiD3GfVM8H8qHIQw+LNRRanXvUFhoHv9KvrOb/ydk6M4o4PDtaox/it7pdMpYTUkkb2PhLrCAwgY28a72gA5jIXeNZ5A7wAa73iAxpMSMQt4t4kIALeJeFoQGLeF4kIAOBjgYyKIgJUMU6XPAr/AE/oSNGnR7E8kjF42hRa2TMXcHcUp2ZltxzWC/eMAPd9h+TDhcBh6LXzBMzA8GqE1HXwZyPCE6CEZEJzm2vIC43CvT3OoLU26nUGw7mBKnv6wJ0cIAfJuFbpN22Ydxtu9nrlkmdBt9slUwNZnVc1Co7FHHormv8AQv8AVIBIHXYEa3A5haw3HTXju7LHcYiQ9zYfrrkLa6wevrpI3qG0AGM/VIosS8YBCLCABC0dTW5tEJgAkBFdbRBABIsQRTAAirEijTfpABZ3/kdpluUVPBaFVj4si/mE4AMLiwJN7CwO87u/wnuvko2NfCI2KrgivVUKEOmSncNZhwYkAkcLAb7wEz0eEIQEEISOo4UXJsIAMxFBailHVWRhYqwBUg7wQdCJ5ptN5PuT2zNRZ6L66U+nTuetGOg36Ky907TlDEs+guF6uv7XymXUw5Mdhni3KeyOIpbgtRRfVCM3HVlcA37Bec/Vw7L6SOv2lK+8T3vEcl5pm1dmFbjFYeh4iT1QnsFbYqm3pAHvUH3iQnYGj9QeoD3Q1A8kgDPWT5PqP1fafnE/6e0fq+0/OAHk97RQdxnrH/T6h9X3/OKNgaP1R4qD74AeTFu2NLdvunrn/IdL6i/gX5R67EoNwA7lUe4Q1A8iRSx6Nz2AX90tpyVXbUI3iVU+okGeqf8AKHaYi7JHrMLMDzbD7N4hz6Kr2tUW38pJ9k6/kHyWmuRmxtAcSqKzPbjo+Q+Np0dLZlh+9LtHkMjidP1oY7AdBszsHg8CQ9NC9Uf5tSzOPsCwVOq6gG28mdZOf5DxTranUYuP3WPpA/VY8e869/DoIiIQhCABKON1IHAe/wDXvl6VWS5vACjzUQ0Ze5uHNxgUeYiGhL/NxOagBQ83h5uJf5qHNQAoebiJ5vNDmoc1ADP83EPN5oc1DmoAZ/m8PNhNDmoc1ADO83HVDzeaPNRObgBn8xF5mX+bhzcAKHNTVw1W667xv+cg5uSUVsYAWoQhEASOEIAEIQgAQhCABCEIAEIQgARDFhAAjRCEAFMSEIAEIQgA6NhCAE0IQgB//9k="
            alt=""
          />

          <Product
            title="Mobile Phones"
            image="https://www.solgroup.net/wp-content/uploads/2020/12/SOL-13.png"
          />
        </div>

        <div className="home_row">
          <Product
            title="Up to 45% off | Furnitures from stores nearby"
            image="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBUVFRgWFhUZGBgaGBkcGBwaGBUYGBkYGRoaGhkYGRgdIS4lHB4rIRoYJjgmKy8xNTU1GiQ7QDs0Py40NjEBDAwMEA8QHxISHjEhISU0NDQxNDQ0NDQ0NDQ0NDQ0NDQ0NDE0MTQ0NDQ0MTQ0NDQ0NDQ0NDE0NDQ0NDE0NDQxNP/AABEIAJ8BPgMBIgACEQEDEQH/xAAcAAEAAQUBAQAAAAAAAAAAAAAABQEDBAYHAgj/xABHEAACAQIDAwgGBggFAwUAAAABAgADEQQSIQUxUQYHEyJBYYGRFDJCcaHBFSNScrGyM2JzgpKis8KDw9Hh8FPS8RYXNENE/8QAGAEBAQEBAQAAAAAAAAAAAAAAAAECAwT/xAAgEQEBAAICAgMBAQAAAAAAAAAAAQIREiExQQMTMmFR/9oADAMBAAIRAxEAPwDs0REBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEpE4hziVqp2jVRGc2WnZVZtBkQ6AHifMyybS3Tt8T55wdd0UdIlZznD731QqQATfcTY2t366CYmLq1iwZBWVWVSBepvAAcjXcWuezeNBLwZ5PpGVnJuaOrU9Iro7ufqkbKzMRq1wbE8D8Z1iSzTUu1YiJFIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiIFJpu2eQ4r4l8SuIem7qoIyI4BUKARf7qn3iblBjaWOY7c5F4mlTatRxTVaq2shp01zC9jYj2rH4AcJmYbkDWygtjnViAWC0qZXNfMbHt6xOu8zD5xMF6SKlhVLUz1dWCKFVWfqbmBBPWte/bpaY/NLtip0jYVmZk6IumY3ylWVSF4Kcw07pJnvpu/HqbbdyX5JDB1KlU1mqM6KpuiIAFtawXt0E2mIlZIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIlIgViJao1VYXU3F2FxxVipHgQR4QLsREBERAREQKShlZF7TAe6H1dLjj22PduhY51tLlTRbFYiiM7vnZECaq6ZRrmvYC5YeF5B829bJtVVOmanVSx7DYNb+SRygUtsAFuqa7oSo+2zACxPYbA+4z3y/wAB6Jikek7AsAwIBUq4J9Vgb33HxnGTWW4727w1fL6EiRPJzaoxOHp1LWLIpYXvqRrr7wZLTs85ERAREQEREBERAREQEivpuiHqI7orIwUgsLm6I4YDfbr28JKzHoYcIzkXu75mvxyImndZB8YGN9M0Oxyfuo7fgplPpml2Cqfdh8Qf7JJRAjvpVeynWP8AgVR+ZRH0k3Zh6x/dRfzOJIykCO+kKnZha3i2GH+bHptY7sM4+9Uoj8rGSUQOF43lBjquK6LEu1MZiGp56lFEUXJa6MrOAoJBzHN4ywKKOKD2dFq4lVVjiHc9CXYfWdcFDbKBaxsjEnVZ2/G7Oo1hlq0kqDg6q1vdcaSHbkRs4m/otO/dmA8gbTUyjPFyddsYjCLTejjKjlioZHIdLmnSqAKrM1x9YU7DdDOubFr42tQp1H6KkzqCUalUZl4X+sFrixt2XtMrZ/J7CUDmpYekjDcwRc38W+Sslu1k0jugxP8A16fhQb51TBwlc78QR92mg/NeSUSKjDgKp/8A1VfBcOP8syo2a3biax8aQ/LTEza1VVUszBVG8sQAPeTulKGIRxdWVhxVgw8xAxPoodtWsf8AFcflIkXVw5wdB6rYl0RDUdgQKykM7MujDOW6wGjC5myzHxVNHRkcKysCGDWykHQgg9kDmeH5xsRWzrSp0VZFZgX6Ul1UMxIRLhbBdbsRcgX1nj/1ntMOqFMOzNT6QIEqX9fo+jJz6Pm0PYO0y9tXk9s7Ds3R7QTCs/suaVXLoy3XN100dhmzbjvl+nzf4sOrfSNyq5VZqJdgmcPbrVDfrWN/DdNdMdq4DnMKOKeMw/RE2OZGDqAwuGI+zbtBM6KjAgEagi49xmi4Pm0oZlbEVqlcqiqostNQFFl0XU27z77zYcTg6ymllqs6CquYMi5guvtJlAUabwffJdelm/adiIkaUkLiGObMdFBJY7gAP97SakFjsIwpvmIK5SDY2Nt1xfdbf4SVZ5aZX2MjYh+iRn6Ry7XOtyc2jaEKGuQDca+Mu8tNk06uFyKo6VDTVDqSpDBG3k6WLb7zNrpSpKL10W9r3dQT3Zrjv98j0c185oMlUoPUVgxLEg6MNNwN/DjOclxlvmu9sysniRu3JrDomHRUN1ChR7lFte/efGTEiOTdJ1ogOCpJJAO8A7r8JLzpLuOF6ulYiJUIiICIiAiIgUJlLieK1JXUqyhgd4YAg+8HQyI5OYKkiOyU0VjWxAJVFU5RXqALcDcLDTugTkoYnJucjlfiKWK9HoVDTCKrMVtmdn1AJI0AFtBxPdLJtnK6m258u+UFTA4bp6aK5DorBs2UK19dO+w8ZoVHnlqW62DQn9WsQPIofxmgbW2lWqG9StUqBt4d2YA7xoTa0jVMsxcss76dUfnhqn1cGg99Vm/BBIXavOPj65XIyUQpzWpg9YjcHZibr3aTTU3XlymZdRi55X2+iaHK7BFFZsRTBKqSMwuCQDYgbj3S3U5bbPU29JQn9UO/xVSJ8+VWKVNNzqD4jQ/gPOWnYrVNu0AycXT7K+hW5d7PBt6Re2+yVWA8QsxH5x8BchajtY2uKb2v42M4S9UrV09pVPju+UVGy1T3gHx3fKOML8ldyXnJwRJsKpA3nIvZ3Zryx/7o4Qi606zDuWn+BecZo1LVWH2gD8vlLdM5ajDsNj57/jLxiX5MnYDzsULFhh6pFtNU+OuksPzsoFLDCsRa4+tGo4nqafGckpPld17L385XCNYMh7GIHuOsmoc8m7csuXpxuG6D0cIHZWzCoX9XUdXIN5nP6RKm69U8QbHzE9JU6pU+ywHk089sumMrd9rz4qod9RyO93PzmMUB36+/WXTPAEJusrZ2B6arToroajogsN2dgt/C959S01sAOAA8p8olyNVJBG4gkEHiCNxm3cn+crG4WyufSKYtdahOcD9Wpv8A4s0mUdPjyk8voSJrXJvlng8aAKVUCoRc0n6tQcQAfWtxW4myzLuREQKSE5VVgtBgRmzkLbfcbzp7hJuaxy3DdEmU2OffofZPHSZy/Nax/UadUwVJlGZF42sNCN1tOwT1gEWg61EGVlvu0uptcNxBsNJieklb5z46D4ZjLtN8/WO72V+bcT3dk88r1Xvy6xRfMqniAfMXlyR2wahahSJ+wB5afKSU9M8PJZ2RESoREQEREBERASM2F+jf9vif69SY+O5UYKiStTE01Yb1DBmHvVbkSH2Ry0wKoytiFU9LWbVXAyvVdlN8ttQQfGXSbblPn7ng6m02bsejSJ/mX+2d02ftShXGajVSoO3Iytb323eM4xz6UQMZRf7VC38Lt/3REvcaW1IMN/w0mHUUqQO2e8HiLDLqTfTWw85lYhQwtYq41AOt+IDDfNPPrSzQbslxby3hRfsmatK2sqLGN1RW7VbX3N/4ExsQ13U8Vt5GSFYKyMoNza/lr/pI6uNU9zfKRYuYhush4qR5GXcV6yHipHlaY2MbVPeflL2NcXTx/AQr1XNmQ8QR/wA85WtpUXvU/A/7yzimtk95nrEP10PvhFMaLVFPEfgZ6DWqfeUH5SmMa7Iff8p5dhnB7LWPnA8uLM3eyn4X+UqFN90EXYneLC3zlw1G7EHiRKl7GSUKaSnpD8B5iXFrn2k+IhNMcLqOEuFL3b3/AA3S4+UkEXFraH/WUpjS3eIVhlCCLXDA3BGhB4gjdOn81fKfGHEJhqtQ1aTBsuclnQqpYZX3ldLWPHSaBRo9Yk6zcubNL7RpdyVD/IR85mxvDK707rESPqbXw6sUbEUg4NipqIGB4ZSb37pl6GfNU5bOLU136sfwHzm1zS+WrfWoP1L+bH/SYz/Lfx/pqf0R6S4pi46rMSOrYKMw3am5yi/fPODclRebfySwtkr1u3KUU8AFzN8SvlNR2ctyq8SB5mcrOo7zLdv8dT2PSy0Ka/qL8Rf5zOnmmtgAOwW8p6nePLVYiJQiIgIiICQXK7Z1bEYWpSoVOjdrWNyoYA3ZCw1AYaXk7EDhWB2dicGtRKuHxAzHQJS6WkSChFQsrhWIynqm+h0KkkzwmKD1arNh69UOiD9HVYNUQMCSpdrLntZSzWt4Tu8jdhfo3/b4n+vUmts8XK+T3JXGPjFr06b4Wmro13BRsuhdFS9yCcwsdLHwjn7pdfCNxWsvkaZH4mdmnKOfqjehhm4VXX+JL/2yW7NajjOHazSVzEq2uo6w9410kRTOokrQO8dxmo4Z+Xu5BOQC51uTprrYSnobv6zW908061lU/qr/AM+ENi2PbKz4X6WzFX2m87fhLrYJDa/Zu1PbMA4g8Z56UmE3Wc+GU77G26UZEO/W264Ews5ngmFZztT7dbbp4dk75h3lC0DJFVOF5Rm/VtMZjpL+Ia7EjjAGAO+W80ZoFwr3ylu+eSx4HynpUY+yYFConnKJf9Gfh8Z59EbivmYGVs9rrqdzW8LXm881FPNjyfs0Kh82pr8zNCoIU0uDe5nSeZmneviH+zTRf42J/sEl8NY/qOvSNrKPSqWn/wBFf8+HnOdpc4OIqYr0elbD0+l6MsafSVb5st8pOUEnstpfUzBr4/G/VVPTXLtUWlYLSBRKtV6YNgNATQU3Isb6bjecXfbs00/lrgXJWsouqrlYDeNSQfdrNLfljtLC5WqVKddGNsrLZhdEqAFlVbEpUQ3GYb50vk5tdcbh0rBCobMCra6qxVgD7QuDrM5Y7jWOWq1fkvtXKKlJxlDBipvcBspBB7jYdm8d8huTAzVkBHtp+Ooktyn2T6O2db9Ex03nKx9n3cJhcmUPpSNrlLaXHq5VJ14DQzh3uSvTNauU9uoiVnkG89Tu8xERAREQEREBERASM2F+jf8Ab4n+vUknIzYX6N/2+J/r1IEnOcc92HzbPVvsYhD/ABK6f3To8g+V2wxjcJUw5bKXylWtfKysGUkdouLHuJhK+WkGokpS3iYuOwb0ar03FnR2VgDcBlNjY9o03zNw41E3HnzY1SqoUL25V+ctBxxlymgNydbmZCIvAQl0xFcT1m4A+RmQ47pTKZU6WLk+yZ6CNwHnLyJra+szcPsmu/qUaj/dpu3xAhUZ0LcQJUUuJmy4fkdj31XC1P3gE/ORLm0uReKw9Fq9VFVFy3GdGbrMEFlUntYSbhxyvprK0QZ76FZumx+QjV6SVTiKdNXFwCruwFyNRoOzjJVeb2iPWxhP3aIH4uY5Rr68v8c4VF+zLiW4CdNw/IbBD1nr1PFEHwUn4yTw/JLALuwzP9+pUPwDAfCOUJ8VcfJtPAra2U37hqfKd2w+x8MnqYKiveaaE+ZElKOdfVREH6qqv4CTks+L+uBUNm4mp6mHrP8AdpVCPMCSVDkbtFvVwlT97In52Bnd0qP2mXwxO+NtfVHz7tbknisNkNZFTOHyjOrHqAE3y3A3idc5vuSrYClUzurvVKMcqkBQq6LcnXeddJF841Ms+FU+01Rf4ujE6Gq2mJlbbHX68cZLPKH2pyYwuIbpHp2qDdURmp1B++hBPjNZxXIKga9On6RispWpUsauazo9MBlLKbHrsb7++dAkdW/+VS/YV/z4ea3U1Gv4bm9wSkF+lrWNwKlRmW9guqiwOgA17AB2TbKNFUUKqhVAsAAAABuAA3S7EmyTTC2ooNF8wDDI2htYkAkb++c95Fh2Gd9wcqjW39W5BPGxP/N/Q9puBTIPtadnbI/Z+EU0XVRY5iRpbr5RY9/ZM3Hd23MrJZPbO2Ofq7cGYfzE/OZ8jNjIwViwIuxNiCDwvY7t0k5pkiIgIiICIiAiIgWa1QqpIVmI9lctz7sxA8zI7YbOFZXoun1lZgWNMgh6ruo6rsb2YSXiAlCZWWnWByDbnNpiMTicTVzpTVqmakScwZXLFgwGqkaW8ZA8muSIxDlXr9GFW9wmYmxAtqwtvPlO4VkPbumicj8IGrV1+xmH87D+2ZuVlkntZhjlLbO4i6PNthl9fFu33aaL+JMkKXIbZy72rv8AvIv5UE3ZNmL9mXk2eB2Cb3WOGP8AjUqPJnZq7sKX++9VvgWtM7D7LwiapgaIPE01J8yDNkXBiXVwokWYyIikzL6lJE+6qr+Al41ax7bSUWgOE9CkOEKhDhqjb3Mh+U/J6tXw7pTa7HL1WNg2Vlbf2HSbrklcsVZdNX2JsHo6FNHUZ1QBrG4zbzY9slE2Yo7BJW0raSTRbu7YCYFeAl0YUcJlWlZUY4oDhPQpDhL0QLeSess9RAxsRhEcqXRWym65lBseIvuMyAJWICYD7JoM+c0wX162t9SCe3iB5CZ8QEREC3UphhZgCOBF5VUAFgLDunuICIiAiIgIiICIiAiIgIiICUIlYgeCsj8BsajRZ3RLNUN3OZjc3LaAnTUndJKJNG1MsBZ6iUUtKxEBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERA//9k="
          />
          <Product
            title="Deals on Red Tape"
            image="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQuY4GiKJ_AYA9Ts7iJsLWa_du2vPp49jFjHA&usqp=CAU"
          />
          <Product
            title="Just launched | Amazon brands & more"
            image="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS2GNLaNP3umpjVe-oG82bN0iAuKj3b4d7f9A&usqp=CAU"
          />
        </div>

        <div className="home_row">
          <Product
            title="Experience the magic of Alexa smart home"
            image="https://images-eu.ssl-images-amazon.com/images/G/31/img19/MAI/Sweepstakes/Nov/MEDH_SWM_1500x200.jpg"
          />
        </div>

        <div className="home_row">
          <Product
            title=" Up to 35% off on Laptops"
            image="https://images-eu.ssl-images-amazon.com/images/G/31/img23/pd23/intel/186x116._SY116_CB601990449_.jpg"
          />
          <Product
            title="Baby diapers & wipes at great prices"
            image="https://images-eu.ssl-images-amazon.com/images/G/31/img22/Baby/cnnjpp1/CC_Rev1x._SY304_CB594433898_.jpg"
          />
          <Product
            title="Best selling toys from smartivity"
            image="https://m.media-amazon.com/images/I/51cJxhsBpaL._AC_SY200_.jpg"
          />
        </div>

        <div className="product_line">
          <div className="product_lineone">
            <img src="https://images-eu.ssl-images-amazon.com/images/G/31/IMG15/PD23/PC_QuadCard_SS_1x_2._SY116_CB601938815_.jpg"></img>

            <div />

            <div className="product_lineonetwo">
              <img src="https://images-eu.ssl-images-amazon.com/images/G/31/home_private_label/moritika/baugwsept_cottonx/xcm_banners_cottons186_186x116_in-en._SY116_CB591200250_.jpg" alt=""></img>
            </div>

            <div>
              <img src="https://images-eu.ssl-images-amazon.com/images/G/31/IMG15/Irshad/PC_QuadCard_LG_Plat_1X._SY116_CB602824374_.jpg"></img>
            </div>

            <div>
              <img src="https://m.media-amazon.com/images/I/81UaMFQR6kL._AC_SY135_.jpg"></img>
            </div>

            <div>
              <img src="https://images-eu.ssl-images-amazon.com/images/G/31/img22/WLA/2023/MSOREFRESHDESKTOP/D87165616_IN_WLA_BAU_MSO_REFRESH-desktop-version_PC_QuadCard_186X116_1X1._SY116_CB602731451_.jpg" alt=""></img>
            </div>

            <div>
              <img src="https://m.media-amazon.com/images/I/61UJocoVpRL._AC_SY200_.jpg"></img>
            </div>
          </div>
          
        </div>
        
        <div className="MiniTV" >
        <Tvmini></Tvmini>
      
      
        </div>
        
      

      </div>
      
    </>
  );
}

export default Home;
