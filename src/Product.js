import React from "react";
import "./Product.css";
function Product({ title, image, price, }) {
  return (
    <div className="product">
      <div className="product_info">
        <strong>{title}</strong>
        <p className="prodct_price">
          <strong>{price}</strong>
        </p>
      </div>
      <img src= {image}  alt="" />
      <a href=""> see more </a>
    </div>
  );
}

export default Product;
