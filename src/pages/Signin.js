import React from "react";

import "./Signin.css";
import { Link } from "react-router-dom";
function Signin() {
  return (
    <>
      <Link to="/">
        <img
          className="BackToHomeLink"
          src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHcAAAAkCAMAAACE/VXhAAAAmVBMVEX///8iHx8AAAD/mQAfHBwcGBgTDg75+fn/lgDs7OyAgIA/PT3/kAC/v7//kwDw8PDi4uJKSUlVVFTR0dEsKyvFxcVycXEMBQU4NjZfXl7Y2Ni5ubmoqKj/9Ov/+vb/r12Qj4+cm5tnZmb/6db/nif/xpP/3L7/iAD/4sn/unj/wISxsLD/1rT/06z/zaL/oDH/pkP/tW3/qlDRWQpQAAAEsklEQVRYha2W2XbiMAxAHWE7ax0gkLATCFCWtpT+/8eNJGdj6JTSqR44Rti62iwjBIp7CvqjcJxPhBV/OBwmQsSrNF35pOiu0mAWi1qegn446gfD+ntbSl18CtJgVZkUCdpECwkqT6VqBaCllEpDz9oOAMUdQqZ1Bmg8B02rabl/qkAr3u9Ys11oi9XlaFThob5vQ6FfcrEmS6C6pFqDgyLpQzsuc7XjwARYJSFe8QZcJaWbstqvoGu5TiOW269U2m7x8Xu2Ki0piZoZrTMYgKJDecXVY2UPqrVlkIp+nABbG4CmH/uWq0nsAeYEtAkjRJVSbsnVgSpNUepoCStX+D3aNKi4GAtAZr3DFWmk5PTgMktj4aakAzLqBiwpgVVY+YbG/TFtDkouWbLh6QC3YOnSOhCouYANkjOYVkEFcanqI054VmfVCmcIqJ7kUrYijzKKKi65Egvrcnhj4c7ytMeH3SuuXtOKc5rXP1Jw0zzt244ckc2kxnLBYFYdYwfEGilwKrncmUNcyV7j7V/cjA2Qc9b0QJbcRsIrLjeX5nonZNqxTYhJ0WnFrSotQ3vGf1rhjVRtLjxVqbOpvOLGySwY96hHGu4It8qML+Isq02fiDIoaVyeuOZOQ+wgrbgdGy4Phb4qG7TNnYxb+ytuDk7lLLeF7XTxVCaRuKp3xR1Ti2XY8/J73Jz2a5o1La69XWnpg665wzK/t1waGxJWXTeGb3Ft+wSJK0LVcKWs7lnF7bW4n8Sb8Njo3vTVv7hxk89WX6VXd4qayRbz33nmOxNUivvcU23AqpI6Kr6wNazsZ2ox2n7Dpa6EU10iTuVX3MZPmx/i8nTAgYqPFodsL01chUAD4IZLybJ31Y6k5A6XRpHimTKtg+SJiWp6tByyxZWf1scmn3DJMteCc2Xb8F68ToazyOf+lzT1B7J5jrBFy2LQqKcHiCf2DTfnN2CUY1uz13TsKy43itRBAJLBNE/5lVFKW0VOu2kIO0G/LsUN176deMaBFMpH9st7ZMPU2gF+IGkg43GAcL0eD/BWM7dLb7TUzdN6e39n/DJJjQbwf4Xk1zNTquTiWLJcjEcxd8IDQyoIMHa7LdPVXyR3uAZuuiTL7D8JsF3uA+YjtFy0RIVNcOyBkzIwtPM17PVCpuW4sn9+UlzYEeSnNKzGlL2TZt6k/V507R8bN6dnG9aVQ3i8xx6JZiWu35n74n5yYF7si/mVxp90H7T7sLwsTWRMFHVef2ph/vz28JkdElGM1/HOP+WK3XPn5cEj21dM8XxeXLzO+4+5ovCizaNkKwvjXX7OFYt3Y7zt/P7GtuyOCzE35vEitWUbeV603H17/3zbiZ4LsTfRg97+LcXGdDzM2m7xDejbGd00eyGO5j/KW8obdieio4/t/gv2fHfoRNjG0YG+bczP+uJKFkc0iGjPmPPhbf93AhfFbrvcROxd9F6Qqog2/49lsjEdFoTj9TwvL4fj8Xg4XD42+NV4Hv8Wfezt/kNU/AoXyVS6Ti1eLbXGeMcKtnj+fiPel2K7MabFbnthzKWFWvxWtJXMXy8bTqvXhG2izsf2t0GfyKJ42R6WH+fz5vy+PBxfb/rsQfkDUV1Q492JnQ0AAAAASUVORK5CYII="
        ></img>
      </Link>

      <div className="login">
        <h1>Sign in</h1>
        <input
          className="inputone"
          type="text"
          placeholder="Email/Phone no."
        ></input>{" "}
        <br></br>
        <input
          className="inputone"
          type="password"
          placeholder="Enter your password"
        />
        <br />
        <button>Continue</button>
      </div>
      <p className="amazontext"> New to Amazon?</p>
      <Link className="createAccountLink" to="https://www.amazon.in/ap/register?showRememberMe=true&openid.pape.max_auth_age=0&openid.identity=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&pageId=inflex&mobileBrowserWeblabTreatment=C&openid.return_to=https%3A%2F%2Fwww.amazon.in%2F%3Fref_%3Dnav_signin&prevRID=K6XPEK3NCT76A6ABMG9E&openid.assoc_handle=inflex&openid.mode=checkid_setup&desktopBrowserWeblabTreatment=C&prepopulatedLoginId=&failedSignInCount=0&openid.claimed_id=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.ns=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0">Create your Amazon account</Link>
    </>
  );
}

export default Signin;
